# This officially released Minetest mod was moved

The repository was moved away from GitLab. The new location can be found here:

* https://git.0x7be.net/dirk/uniham

Just set the new origin in your local copy of the repository.

Also make sure to change `master` branch to `main` branch. The `master` branch is no longer in use and was renamed.

## Mod in Content Database

If you use the [Minetest content database][cdb] just wait for the update to show up in the client or manually check for updates. The CDB link and package name stay the same.

* https://content.minetest.net/packages/Linuxdirk/uniham/

The move only affects Git pulls from the repository.


[cdb]: https://content.minetest.net
